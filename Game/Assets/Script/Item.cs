using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public 
    bool isInteractive;

    void Start(){
        isInteractive = false;
    }

    void Update(){
        if(Input.GetButtonDown("PickUp") && isInteractive){
            Destroy(gameObject);
        }
    }

    void OnMouseEnter(){
        isInteractive = true;
    }

    void OnMouseExit(){
        isInteractive = false;
    }
    
    void OnGUI(){
        if(isInteractive){
            GUI.Button(new Rect(Screen.width/2 - 10 , Screen.height/2 + 10, 20, 20),"E");
        }
    }
}
