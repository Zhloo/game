using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 100f;  // 鼠标灵敏度

    public Transform playerBody;

    float xRotation = 0f;  // 

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;  // 将鼠标隐藏并锁定在屏幕中心
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;  // 获取鼠标在x轴的移动方向
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;  // 获取鼠标在y轴的移动方向

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -70f, 70f);  // 限制上下旋转的角度

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);  // 旋转相机
        playerBody.Rotate(Vector3.up * mouseX);  // 旋转人物模型

    }
}
